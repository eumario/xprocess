/*
################################################@
# XProcess -- IO Peek function (Win/*nix)       @
# Version: 0.1  Beta                            @
# Created by: Mario Steele                      @
################################################@
# This code allows for reading asynchronously   @
# from a normal file handle, such as thoes      @
# created as Redirected Input/Output file       @
# handles, such as stdout/stderr                @
################################################@
*/

#include <ruby.h>
#include <rubyio.h>

static VALUE partial_read(VALUE io)
{
  return rb_funcall(io,rb_intern("read_nonblock"),1,INT2FIX(1));
}

static VALUE partial_except(VALUE unused)
{
  return Qnil;
}

static VALUE io_peek(VALUE self)
{
#ifdef __WINDOWS__
  OpenFile* vfd;
  GetOpenFile(self, vfd);
  FILE *fd = GetReadFile(vfd);
  long osf = get_osfhandle(fileno(fd));
  DWORD avail = 0;
  BOOL res = PeekNamedPipe((HANDLE)osf, NULL, 0, NULL, &avail, NULL);
  if (!res) {
    return Qnil;
  } else if (avail > 0) {
    DWORD read_bytes = 0;
    char* buf = (char*)malloc(avail);
    ReadFile((HANDLE)osf, buf, avail, &read_bytes, NULL);
    VALUE res = rb_str_new(buf, read_bytes);
    free(buf);
    return res;
  } else {
    return rb_str_new(0, 0);
  }
#else
  VALUE read, write, err, timeout, ret;
  read = rb_ary_new();
  rb_ary_push(read,self);
  write = rb_ary_new();
  err = rb_ary_new();
  timeout = rb_float_new(0.001);
  ret = rb_funcall(rb_mKernel,rb_intern("select"),4,read,write,err,timeout);
  if(NIL_P(ret))
    return rb_str_new(0,0);
  else
  {
    if (rb_funcall(self,rb_intern("eof?"),0) == Qtrue)
      return Qnil;
    else
    {
      ret = rb_str_new(0,0);
      do
      {
        write = rb_rescue(partial_read,self,partial_except,Qnil);
        if (!NIL_P(write))
          rb_str_append(ret,write);
      } while(!NIL_P(write));
      return ret;
    }
  }
#endif
}

void Init_io_peek() {
  rb_define_method(rb_cIO,"peek",io_peek,0);
}
