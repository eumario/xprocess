require 'mkmf'

dir_config( "." )

if have_header( "ruby.h" ) && have_header( "rubyio.h" )
  if PLATFORM =~ /mingw|mswin|bccwin/
    $CFLAGS << "-D__WINDOWS__"
  end
  create_makefile("io_peek")
end