/*
################################################@
# XProcess -- Win32 Core                        @
# Version: 0.1  Beta                            @
# Created by: Mario Steele                      @
################################################@
# This is created for Windows based OSes, to    @
# allow process execution to be accessed from a @
# single API.                                   @
################################################@
*/
#include <ruby.h>
#include <rubysig.h>
#include <rubyio.h>

VALUE rb_cXProcess;
VALUE rb_mOpen3;
VALUE rb_cPipeIO;

static VALUE xproc_init(VALUE self, VALUE args)
{
  rb_iv_set(self,"@cmd",rb_ary_pop(args));
  rb_iv_set(self,"@in",Qnil);
  rb_iv_set(self,"@out",Qnil);
  rb_iv_set(self,"@err",Qnil);
  rb_iv_set(self,"@pid",Qnil);
  return self;
}

static VALUE xproc_execute(VALUE self, VALUE args)
{
  VALUE ARGV[1] = {Qnil};
  VALUE temp;
  VALUE arg_str = rb_funcall(args,rb_intern("join"),1,rb_str_new(" ",1));
  rb_funcall(arg_str,rb_intern("reverse!"),0);
  arg_str = rb_funcall(arg_str,rb_intern("+"),1,rb_str_new(" ",1));
  rb_funcall(arg_str,rb_intern("reverse!"),0);
  VALUE cmd = rb_funcall(rb_iv_get(self,"@cmd"),rb_intern("+"),1,arg_str);
  VALUE fids = rb_funcall(rb_mOpen3,rb_intern("popen3"),3,cmd,rb_str_new("t",1),Qtrue);
  rb_iv_set(self,"@pid",rb_ary_pop(fids));
  ARGV[0] = rb_ary_pop(fids);
  rb_iv_set(self,"@err",rb_class_new_instance(1,ARGV,rb_cPipeIO));
  ARGV[0] = rb_ary_pop(fids);
  rb_iv_set(self,"@out",rb_class_new_instance(1,ARGV,rb_cPipeIO));
  ARGV[0] = rb_ary_pop(fids);
  //rb_iv_set(self,"@in",rb_class_new_instance(1,ARGV,rb_cPipeIO));
  rb_iv_set(self,"@in",ARGV[0]);
  return Qnil;
}

static VALUE xproc_exist(VALUE self, VALUE args)
{
  VALUE pid = rb_iv_get(self,"@pid");
  if(NIL_P(pid))
    return Qnil;
  HANDLE ph = OpenProcess(PROCESS_QUERY_INFORMATION,0,FIX2INT(pid));
  if(ph==0)
    return Qfalse;
  else
  {
    int exitstatus;
    GetExitCodeProcess(ph,&exitstatus);
    if (exitstatus == STILL_ACTIVE)
      return Qtrue;
    else
      return Qfalse;
    CloseHandle(ph);
  }
}

static VALUE xproc_kill(VALUE self, VALUE args)
{
  VALUE pid = rb_iv_get(self,"@pid");
  if(NIL_P(pid))
    return Qnil;
  HANDLE ph = OpenProcess(PROCESS_TERMINATE,0,FIX2INT(pid));
  if(ph=0)
    return Qfalse;
  else
  {
    if(TerminateProcess(ph,255) == 0)
    {
      CloseHandle(ph);
      return Qfalse;
    }
    else
    {
      CloseHandle(ph);
      return Qtrue;
    }
  }
}

static VALUE xproc_close(VALUE self, VALUE args)
{
  VALUE io;
  io = rb_iv_get(self,"@in");
  if(!NIL_P(io))
    if (rb_funcall(io,rb_intern("closed?"),0) == Qfalse)
      rb_funcall(io,rb_intern("close"),0);
  io = rb_iv_get(self,"@out");
  if (!NIL_P(io))
    if (rb_funcall(io,rb_intern("closed?"),0) == Qfalse)
      rb_funcall(io,rb_intern("close"),0);
  io = rb_iv_get(self,"@err");
  if (!NIL_P(io))
    if (rb_funcall(io,rb_intern("closed?"),0) == Qfalse)
      rb_funcall(io,rb_intern("close"),0);
  rb_iv_set(self,"@in",Qnil);
  rb_iv_set(self,"@out",Qnil);
  rb_iv_set(self,"@err",Qnil);
  rb_iv_set(self,"@pid",Qnil);
}

static VALUE xproc_popen(VALUE self, VALUE args)
{
  VALUE nargs = rb_funcall(args,rb_intern("join"),1,rb_str_new(" ",1));
  VALUE cmd = rb_funcall(args,rb_intern("split"),2,rb_str_new(" ",1),INT2FIX(2));
  args = rb_ary_pop(cmd);
  cmd = rb_ary_pop(cmd);
  VALUE nself = rb_class_new_instance(1,cmd,rb_cXProcess);
  xproc_execute(nself,args);
  return nself;
}

void Init_xproc_win()
{
  rb_mOpen3 = rb_const_get(rb_cObject,rb_intern("Open3"));
  rb_cPipeIO = rb_const_get(rb_cObject,rb_intern("PipeIO"));
  rb_cXProcess = rb_define_class("XProcess",rb_cObject);
  rb_define_singleton_method(rb_cXProcess,"popen",xproc_popen,-2);
  rb_define_method(rb_cXProcess,"initialize",xproc_init,-2);
  rb_define_method(rb_cXProcess,"execute",xproc_execute,-2);
  rb_define_method(rb_cXProcess,"exist?",xproc_exist,-2);
  rb_define_method(rb_cXProcess,"kill",xproc_exist,-2);
  rb_define_method(rb_cXProcess,"close",xproc_close,-2);
  rb_define_attr(rb_cXProcess,"pid",1,0);
  rb_define_attr(rb_cXProcess,"in",1,0);
  rb_define_attr(rb_cXProcess,"out",1,0);
  rb_define_attr(rb_cXProcess,"err",1,0);
}
