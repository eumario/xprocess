require 'mkmf'

if RUBY_PLATFORM =~ /mingw|mswin|bccwin/
  if have_header('ruby.h') && have_header('rubyio.h')
    create_makefile("xproc_win")
  end
else
  puts "creating Makefile"
  File.open("Makefile","w") do |fh|
    fh.puts <<EOMF
xproc_win:
	echo "No Compile" > xproc_win.so
clean:
	rm -rf xproc_win.so
distclean:  clean
	rm -rf Makefile
EOMF
  end
end
