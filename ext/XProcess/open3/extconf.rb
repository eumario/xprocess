require 'mkmf'

if RUBY_PLATFORM =~ /mingw|mswin|bccwin/
  have_type('rb_pid_t', 'ruby.h') # For 1.8.3 and later
  create_makefile('open3')
else
  puts "creating Makefile"
  File.open("Makefile","w") do |fh|
    fh.puts <<EOMF
open3:
	echo "No Compile" > open3.so
clean:
	rm -rf open3.so
distclean: clean
	rm -rf Makefile
EOMF
  end
end
