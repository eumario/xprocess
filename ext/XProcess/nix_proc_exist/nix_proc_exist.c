/*
################################################@
# XProcess -- Process Exist (Unix)              @
# Version: 0.1  Beta                            @
# Created by: Mario Steele                      @
################################################@
# This code checks to see if the process still  @
# exists on Unix based systems, such as Linux   @
# and MacOS X.                                  @
################################################@
*/

#include <ruby.h>
#include <rubysig.h>

static VALUE nix_proc_exist(VALUE self, VALUE args)
{
	VALUE pid = rb_ary_pop(args);
	if (kill(FIX2INT(pid), 0) == -1 && errno == ESRCH)
		return Qfalse;
	else
		return Qtrue;
}

void Init_nix_proc_exist()
{
	rb_define_singleton_method(rb_mProcess,"exist?",nix_proc_exist,-2);
}
