#--
################################################@
# XProcess -- Core Library                      @
# Version: 0.1  Beta                            @
# Created by: Mario Steele                      @
################################################@
# This library merges together the xproc_win32  @
# and xproc_nix, into a single API interface    @
# depending upon which OS is being used at the  @
# the time.                                     @
################################################@
#++

if RUBY_PLATFORM =~ /mingw|mswin|bccwin|linux/
  ext = "so"
else
  ext = "bundle"
end

require "XProcess/io_peek.#{ext}"
require 'XProcess/PipeIO.rb'

if RUBY_PLATFORM =~ /mingw|mswin|bccwin/
  require "XProcess/open3.#{ext}"
  require "XProcess/xproc_win.#{ext}"
else
  require "XProcess/nix_proc_exist.#{ext}"
  require 'XProcess/open4.rb'
  require 'XProcess/xproc_nix.rb'
end

#--
# RDoc documentation to follow
#++