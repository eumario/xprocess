################################################@
# XProcess -- Unix Core                         @
# Version: 0.1  Beta                            @
# Created by: Mario Steele                      @
################################################@
# This is created for Linux and Mac OS X, to    @
# allow process execution to be accessed from a @
# single API.                                   @
################################################@

class XProcess
  attr_reader :in, :out, :err, :pid
  
  def XProcess.popen(cmd,*args)
    obj = XProcess.new(cmd)
    obj.execute(args)
    return obj
  end
  
  def initialize(cmd)
    @cmd = cmd
    @in = nil
    @out = nil
    @err = nil
    @pid = nil
  end
  
  def execute(*args)
    args = args.join(" ")
    args = " " + args
    @pid, @in, @out, @err = Open4::popen4(@cmd + args)
    @in = @in
    @out = PipeIO.new(@out)
    @err = PipeIO.new(@err)
    Process.detach(@pid)
    nil
  end
  
  def exist?
    Process.exist?(@pid)
  end
  
  def kill(sig)
    Process.kill(sig,@pid)
  end
  
  def close
    @in.close
    @out.close
    @err.close
    @in, @out, @err, @pid = [nil,nil,nil,nil]
    nil
  end
end
