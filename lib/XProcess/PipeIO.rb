#--
################################################@
# XProcess -- Core Library                      @
# Version: 0.1  Beta                            @
# Created by: Mario Steele                      @
################################################@
# This library merges together the xproc_win32  @
# and xproc_nix, into a single API interface    @
# depending upon which OS is being used at the  @
# the time.                                     @
################################################@
#++

require 'delegate'
require 'stringio'

class PipeIO < DelegateClass(StringIO)
  def initialize(io)
    @buffer = StringIO.new("")
    @io = io
    @eof = false
    super(@buffer)
  end
  
  def is_data_available?
    oldpos = tell
    temp = @io.peek
    if temp != "" and !temp.nil?
      @buffer << temp
    elsif temp.nil?
      @eof = true
    end
    seek(oldpos)
    if ((size - tell) > 0)
      @eof = false
    end
    ((size - tell) > 0)
  end
  
  def eof?
    data = data_avail?
    if data
      false
    else
      @eof
    end
  end
  
  def close
    @io.close
    @buffer.close
  end
  
  alias :is_data_avail? :is_data_available?
  alias :data_available? :is_data_available?
  alias :data_avail? :is_data_available?
end
