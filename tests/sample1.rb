################
# Example 1
################

require 'XProcess'

cmd = XProcess.new("ruby")

script = <<EOS
STDOUT.sync = true
STDERR.sync = true
10.downto(0) do |i|
  printf('%d...',i)
  sleep(1)
  puts 'Boom!' if i == 0
end
1/0
EOS

puts "Script:"
puts script

script = script.split("\n").join("; ")

puts "Executing script..."
cmd.execute("-e \"#{script}\"")

loop do
  if cmd.out.data_avail?
    puts "stdout> #{cmd.out.read}"
  end
  if cmd.err.data_avail?
    if cmd.out.data_avail?
      puts "stdout> #{cmd.out.read}"
    end
    puts "stderr> #{cmd.err.read}"
  end
  unless cmd.exist?
    if cmd.out.eof? && cmd.err.eof?
      break
    end
  end
  sleep(0.001)
end
puts "Execution complete."