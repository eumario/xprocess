################
# Example 1
################

require 'XProcess'

cmd = XProcess.new("ping")
if ARGV.length != 0
  arg = ARGV[0]
else
  puts "An argument is expected."
  puts "Example: ruby sample2.rb www.google.com"
  exit!(-1)
end

puts "Executing ping..."
if RUBY_PLATFORM =~ /linux|darwin/
  cmd.execute("-c 4",arg)
else
  cmd.execute("-n 4",arg)
end

loop do
  if cmd.out.data_avail?
    puts "[.]ping> #{cmd.out.gets}"
  end
  if cmd.err.data_avail?
    if cmd.out.data_avail?
      puts "[.]ping> #{cmd.out.gets}"
    end
    puts "[!]ping> #{cmd.err.read}"
  end
  unless cmd.exist?
    if cmd.out.eof? && cmd.err.eof?
      break
    end
  end
  sleep(0.001)
end
puts "Execution complete."